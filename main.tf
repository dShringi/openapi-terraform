locals {
  aws_region = "eu-west-2"
  account_id = "654043815235"
  vpc_id = "vpc-9e85f6f6"
  environment_url = "http://18.132.2.242:8080/"
}

resource "null_resource" "clone-spec-file" {
  triggers {
    always_run = timestamp()
  }

  provisioner "local-exec" {
    command = "git clone https://gitlab.com/dShringi/openapi-spec.git"
  }
}
data "template_file" swagger {
  template = file("./openapi-spec/api-spec.yaml")
  vars = {
    aws_region = local.aws_region
    account_id = local.account_id
    vpc_id = local.vpc_id
    environment_url = local.environment_url
  }
  depends_on = ["null_resource.clone-spec-file"]
}
resource "aws_api_gateway_rest_api" "POC-APIG" {
  name = "BSA Notify"
  api_key_source = "HEADER"
  body = data.template_file.swagger.rendered
  endpoint_configuration {
    types = ["PRIVATE"]
  }
}
resource "aws_api_gateway_stage" "POC-OPENAPI-STG" {
  stage_name    = "poc-openapi-stg"
  rest_api_id   = aws_api_gateway_rest_api.POC-APIG.id
  deployment_id = aws_api_gateway_deployment.POC-APIG.id
}
resource "aws_api_gateway_deployment" "POC-APIG" {
  rest_api_id = aws_api_gateway_rest_api.POC-APIG.id
  stage_name  = "dev-bsa-notify"
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_api_gateway_api_key" "POC-APIG-KEY" {
  name = "notify-api-key"
}
resource "aws_api_gateway_usage_plan" "POC-APIG-USG" {
  name         = "common-components-usage-plan"
  description  = "common components usage plan"

  api_stages {
    api_id = aws_api_gateway_rest_api.POC-APIG.id
    stage  = aws_api_gateway_stage.POC-OPENAPI-STG.stage_name
  }
}
resource "aws_api_gateway_usage_plan_key" "POC-APIG-USG" {
  key_id        = aws_api_gateway_api_key.POC-APIG-KEY.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.POC-APIG-USG.id
}
